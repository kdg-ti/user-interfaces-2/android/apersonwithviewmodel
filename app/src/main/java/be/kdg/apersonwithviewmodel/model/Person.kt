package be.kdg.personsrest.model

import com.google.gson.annotations.SerializedName

data class Person(
    val id: Int,
    @SerializedName("firstName")
    val givenName: String,
    val married: Boolean
)
