package be.kdg.personsrest.rest

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import be.kdg.personsrest.model.Person
import com.google.gson.GsonBuilder
import io.reactivex.rxjava3.core.Observable
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

// De BASE_URL hebben we hier als constante gedefinieerd.
const val BASE_URL = "http://10.0.2.2:3000"

/*
 * We schrijven een aparte klasse RestClient waarin alle netwerk operaties gebeuren
 */
class RestClient(private val context: Context) {

	/*
	 * In deze methode wordt een connectie gelegd op basis van een urlString
	 * Check AndroidManifest.xml: om een connectie te kunnen leggen moeten
	 * een aantal permissies ingesteld worden!
	 */

  /** @throws IOException
   * Kotlin indicates exceptions being thrown only in KDoc (javadoc)
   */
	private fun connect(urlString: String, method: String="GET"): HttpURLConnection {
		val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    // check if IP connectivity OK
    val capability = connMgr.getNetworkCapabilities(connMgr.activeNetwork)
		if (capability != null && capability.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)) {
			val url = URL(urlString)
			val connection = url.openConnection() as HttpURLConnection
			// De Kotlin apply functie laat toe om eenvoudig een aantal
			// functies toe te passen op een object...
			connection.apply {
				connectTimeout = 15000 // We wachten maximaal 15 seconden op een connectie.
				readTimeout = 10000 // Bij elke request wachten we max. 10 sec. op een antwoord. default = 0 (eeuwig)
				requestMethod = method // default is "GET"
				connect()
				return this
			}
		}
    // if we have succesfully returned the connection we will not reach this statement
		throw IOException("Unable to connect to network")
	}

	// Voor elke Restcall maken we hier een methode.
	// Aangezien de Restcall een tijd kan duren returnen we een Observable.
	// Deze kan dan op een andere thread dan de main thread runnen.
	fun getPersons(): Observable<Array<Person>> {
		// We maken een Observable aan met één van de static create methodes van de Observable klasse
		val observable = Observable.create<Array<Person>> { emitter ->
			try {
				val connection = connect("${BASE_URL}/persons")
				// We gebruiken gson om de JSON data om te zetten naar een Array van Person objecten.
				// Opgelet: de velden van onze Person modelklasse moeten dan overeenkomen met de JSON velden!
				// Zie de cursus Java2 voor meer informatie over het gebruik van gson.
				val gson = GsonBuilder().create()
				val persons = gson.fromJson(
					InputStreamReader(connection.inputStream),
					Array<Person>::class.java
				)
				// Hebben we alle persons binnegehaald, dan melden we dit aan de Observer(s) via de onNext methode...
				// alle personen worden hier samen doorgegeven
				// er zijn ook implementaties mogelijk die voor elke afzonderlijk item onNext aanroepen https://github.com/ReactiveX/RxJava#runtime
				emitter.onNext(persons)
			} catch (e: Exception) {
				// Liep er iets mis bij het connecteren (we kregen bijvoorbeeld een timeout),
				// dan melden we dat aan de Observer(s) via de onError methode
				emitter.onError(e)
			}
		}

		return observable
	}

	fun getPerson(id: Int): Observable<Person> {
		val observable = Observable.create<Person> { emitter ->
			try {
				val connection = connect("${BASE_URL}/persons/$id")
				val gson = GsonBuilder().create()
				val person = gson.fromJson(
					InputStreamReader(connection.inputStream),
					Person::class.java
				)
				emitter.onNext(person)
			} catch (e: Exception) {
				emitter.onError(e)
			}
		}
		return observable
	}
}