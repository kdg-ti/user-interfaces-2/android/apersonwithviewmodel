package be.kdg.apersonwithviewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import be.kdg.personsrest.rest.RestClient
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

//We ervan van de klasse ViewModel
class PersonViewModel: ViewModel() {
    val name = MutableLiveData<String>()
    val married = MutableLiveData<Boolean>()
    private lateinit var disposable: Disposable

    init {
        Log.i("mylogs", "PersonViewModel created...")
    }

    //Het ViewModel wordt alleen vernietigd wanneer de volledige app afgesloten wordt
    //En dus niet bij bijvoorbeeld een configuration change van de Activity of het Fragment
    override fun onCleared() {
        super.onCleared()
        Log.i("mylogs", "PersonViewModel destroyed...")
        disposable.dispose()
    }

    fun loadAPerson(context: Context, id: Int) {
        disposable = RestClient(context)
            .getPerson(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    //We passen de values van de MutableLiveData objecten aan, de UI zal automatisch ge-update worden!
                    this.name.value = it.givenName
                    this.married.value = it.married
                },
                {
                    Log.e("mylogs",it.message!!)
                })
    }
}