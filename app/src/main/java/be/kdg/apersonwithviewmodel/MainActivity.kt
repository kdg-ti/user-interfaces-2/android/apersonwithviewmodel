package be.kdg.apersonwithviewmodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import be.kdg.apersonwithviewmodel.databinding.ActivityMainBinding

//De Activity (of het Fragment) noemen we ook wel de "UI Controller"
//Een UI Controller moet eenvoudig blijven: hij toont de data in de view en vangt events op, meer niet
class MainActivity : AppCompatActivity() {
    //We werken nog steeds met viewBinding.
    //Je kan ook een stap verder gaan en databinding gebruiken, zie https://developer.android.com/topic/libraries/data-binding
    private lateinit var binding: ActivityMainBinding

    //Het ViewModel bevat de data die de UI Controller moet tonen
    private lateinit var viewModel: PersonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //De UI Controller maakt nooit zelf het viewmodel aan, hij vraagt het aan de ViewModelProvider
        //Deze zal ervoor zorgen dat, als het viewmodel al bestaat, het niet opnieuw gecreëerd wordt
        //Op die manier blijft de data dus behouden bij een destroy van de Activity/Fragment
        viewModel = ViewModelProvider(this).get(PersonViewModel::class.java)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //In het viewmodel maken we gebruik van LiveData attributen
        //Deze implementeren het Observer design pattern en melden zelf wanneer ze gewijzigd zijn
        //Wanneer ze wijzigen passen we de UI aan
        //(Dit kan nog eenvoudiger door gebruik te maken van dataBinding ipv viewBinding)
        viewModel.name.observe(this, Observer {
            binding.tvName.text = it
        })
        viewModel.married.observe(this, Observer {
            binding.cbMarried.isChecked = it
        })

        //UI Controller vangt events op en sluist door naar de juiste methodes in het ViewModel
        binding.btnGet.setOnClickListener {
            viewModel.loadAPerson(this, Integer.parseInt(binding.etID.text.toString()))
        }
    }
}